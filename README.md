Config Reference
===

## Setup
```bash
- install virtualenv with python3.6 (virtualenv --python=`which python3.6` .env)
- activate virtualenv (source .env/bin/activate)
- install requirements (pip install -r requirements/production.txt)
```

## Run
```bash
- activate virtualenv (source .env/bin/activate)
- export FLASK_APP=run.py
- flask run
```

## Enable debug
```bash
export FLASK_DEBUG=1
```

## Docs
```bash
/swagger/
```