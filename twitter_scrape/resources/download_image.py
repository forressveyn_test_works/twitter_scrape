import os
from os import listdir
from os.path import isfile, join
from flask import send_from_directory, abort, current_app
from flask.views import MethodView

__all__ = ['DownloadImage']

img_dir = './data'


class DownloadImage(MethodView):

    def get(self, handle):

        handle = handle.lower()

        for filename in listdir(img_dir):
            if isfile(join(img_dir, filename)):

                name, extension = os.path.splitext(filename)

                if name == handle:
                    return send_from_directory(
                        img_dir, filename, as_attachment=True
                    )

        abort(404)
