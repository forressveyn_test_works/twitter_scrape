import os
from os import listdir
from os.path import isfile, join
from flask_restful import Resource

__all__ = ['HandleList']

img_dir = './data'


class HandleList(Resource):

    def get(self):

        onlyfiles = [
            os.path.splitext(filename)[0]
            for filename in listdir(img_dir)
            if isfile(join(img_dir, filename))
        ]

        return {
            'status': 'success',
            'response': {
                'users': onlyfiles,
            },
        }
