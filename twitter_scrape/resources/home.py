from flask_restful import Resource


__all__ = ['Home']


class Home(Resource):

    def get(self):
        return {'name': 'twitter scrape api'}
