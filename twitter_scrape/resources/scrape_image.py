import os
import requests
import ntpath

from flask_restful import Resource, reqparse
from bs4 import BeautifulSoup

parser = reqparse.RequestParser()
parser.add_argument(
    'handle', help='This field cannot be blank', required=True
)

__all__ = ['ScrapeImage']

img_dir = './data'


class ScrapeImage(Resource):

    def post(self):

        data = parser.parse_args()

        handle = data.get('handle').lower()

        profile_link = f'https://twitter.com/{handle}/'
        page = requests.get(profile_link)

        if page.status_code == 404:
            return {
                'status': 'error',
                'response': {
                    'error_msg': 'User does not exists'
                },
            }
        elif page.status_code != 200:
            return {
                'status': 'error',
                'response': {
                    'error_msg': 'Some parsing error'
                },
            }

        soup = BeautifulSoup(page.content, 'html.parser')

        soup_result = soup.find('img', {'class': 'ProfileAvatar-image'})

        if soup_result and soup_result.attrs and soup_result.attrs.get('src'):
            link = soup_result.attrs.get('src')
        else:
            return {
                'status': 'error',
                'response': {
                    'error_msg': 'Some scrapping error. Update required'
                },
            }

        _, filename = ntpath.split(link)
        _, extension = os.path.splitext(filename)

        new_filename = f'{handle}{extension}'

        image_response = requests.get(link)

        with open(f'{img_dir}/{new_filename}', 'wb') as f:
            f.write(image_response.content)

        return {
            'status': 'success',
            'response': {
                'filename': new_filename,
            },
        }
