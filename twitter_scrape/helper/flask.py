import os
import yaml
from flask import Flask as BaseFlask, Config as BaseConfig

__all__ = ['Config', 'Flask']


class Config(BaseConfig):
    def from_yaml(self, root_path):
        env = os.environ.get('FLASK_ENV', 'production').upper()
        self['ENVIRONMENT'] = env.lower()
        for fn in ('app', 'parameters'):
            config_file = os.path.join(root_path, 'config', '%s.yml' % fn)
            try:
                with open(config_file) as f:
                    c = yaml.load(f)
                c = c.get(env, c)
                for key in c.keys():
                    if key.isupper():
                        self[key] = c[key]
            except Exception as ex:
                pass


class Flask(BaseFlask):
    """Extended version of `Flask` that implements custom config class
    and adds `register_middleware` method"""
    def make_config(self, instance_relative=False):
        root_path = self.root_path
        if instance_relative:
            root_path = self.instance_path
        return Config(root_path, self.default_config)
