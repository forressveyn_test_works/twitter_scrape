import logging

from flask_restful import Api

from twitter_scrape.helper import Flask
from twitter_scrape.resources import Home
from twitter_scrape.resources import ScrapeImage
from twitter_scrape.resources import HandleList
from twitter_scrape.resources import DownloadImage
from flask import render_template


# from twitter_scrape.resources.change_password import ChangePassword
# from twitter_scrape.resources.home import Home

app = Flask(__name__)
api = Api(app)

app.config.from_yaml(f'{app.root_path}/twitter_scrape')


LOG_FORMAT = '%(levelname) -10s %(asctime)s %(name) -30s %(funcName) ' \
             '-35s %(lineno) -5d: %(message)s'

if app.config['DEBUG']:
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
else:
    logging.basicConfig(level=logging.WARNING, format=LOG_FORMAT)


api.add_resource(Home, '/')
api.add_resource(ScrapeImage, '/scrape/')
api.add_resource(HandleList, '/users/')
app.add_url_rule(
    '/user/<string:handle>/profile_pic/',
    view_func=DownloadImage.as_view('download_image')
)


@app.route('/swagger/')
def hello(name=None):
    return render_template('docs/Swagger UI.html', name=name)


if __name__ == '__main__':
    app.run()
